# Hi, I'm Seyha
I'm a software engineer who is passionate about software development.

## Find me on the web 🌍
- 🌐 Personal website with blog on [cortana.at](https://)
- 👨‍💻 Member of [EveryoneCanContribute](https://)

## Connect with me
<a href="https://twitter.com">
    <img alt="tonka3000 | Twitter" align="left" width="26px" style="margin-right:15px" src="https://raw.githubusercontent.com/tonka3000/tonka3000/master/assets/twitter.svg" />
  </a>
<a href="https://gitlab.com/tonka3000">
  <img alt="tonka3000 | Twitter" align="left" width="26px" style="margin-right:15px" src="https://raw.githubusercontent.com/tonka3000/tonka3000/master/assets/gitlab.svg" />
</a>

<br/>


